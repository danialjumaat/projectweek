<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" 
    crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Wiki over je favoriet spel</title>
  </head>
  <body>


    <div class="container">
    <nav class="navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://warthunder.com/en">Link</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Extra Info
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#Carousel">Carousel</a>
              <a class="dropdown-item" href="#">Sign up</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>

    <div class="card mb-1">
      <img src="./MicrosoftTeams-image (35).png" class="card-img-top" alt="">
      
    </div>

    <div class="text-light mb-1 bg-danger jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="text-center display-4">War Thunder</h1>
        <p class="mr-4">In this website you'll learn more about the game named War Thunder, a realistic war game where you play as military vehicles against other players.</p>
      </div>
    </div>

    <div id="carouselExampleControls" class="container mb-1 pl-0 pr-0 carousel slide" data-ride="carousel">
      <div class="carousel-inner"id="Carousel">
        <div class="carousel-item active">
          <img src="./tank.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="./newpower.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="./gepard.jpg" class="d-block w-100" alt="...">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

<!--     
    <div class="row">
      <div class="col-6">
        <form action="./create.php" method="post">
         <div class="form-group">
           <label for="lastname">Username</label>
           <input type="text" class="form-control" id="username" aria-describedby="usernameHelp"
           placeholder="Je achternaam graag invoeren" name="username">
           <div class="form-group">
           <label for="email">E-mailadres</label>
           <input type="email" class="form-control" id="email" aria-describedby="emailHelp"
           placeholder="Je e-mailadres graag invoeren " name="email">
           </div>

           <div class="form-group">
           <label for="password">Wachtwoord</label>
           <input type="password" class="form-control" id="password" aria-describedby="passwordHelp"
           placeholder="Je wachtwoord graag invoeren " name="password">
           </div> -->


           <form action="login.php" method="post">
            <h2>LOGIN</h2>
            <?php if (isset($_GET['error'])) { ?>
                <p class="error"id="bg"><?php echo $_GET['error']; ?></p>
            <?php } ?>
            <label>User Name</label>
            <input type="text" name="uname" placeholder="User Name">
    
            <label>Password</label>
            <input type="password" name="password" placeholder="Password">
            
            <button type="sumbit">Login</button>
            </form>



            <form action="registration.php" method="post">
            <h2>REGISTRATION</h2>
            <?php if (isset($_GET['error'])) { ?>
                <p class="error"id="bg"><?php echo $_GET['error']; ?></p>
            <?php } ?>
            <label>User Name</label>
            <input type="text" name="runame" placeholder="User Name">
    
            <label>Password</label>
            <input type="password" name="rpassword" placeholder="Password">

            <label>E-Mail</label>
            <input type="email" name="email" placeholder="Email">
            
            <button type="sumbit">Register</button>
            </form>




           <!-- <small id="emailHelp" class="form-text text-dark">We zullen uw gegevens nooit delen met derden.</small>
          </div>
         <button type="submit" class="btn btn-primary">Versturen</button>
           </form>
       </div>
     -->
    
 

  </div>


   



<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
     integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
    crossorigin="anonymous"></script>
    <script src="./js/app.js"></script>
  </body>
</html>